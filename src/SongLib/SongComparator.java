/*
 * 
 * @authors: Chris Hartley and Jerry Zhu
 * 
 */
package SongLib;

import java.util.Comparator;

public class SongComparator implements Comparator<Song> {

	
	@Override
	public int compare(Song song1, Song song2) {
		
		//compare the two songs together
		//alphabetical order and if the songs are the same than the artist will be decide which comes first
		int result;
		//if the song titles are the same
		result = song1.name.toLowerCase().compareTo(song2.name.toLowerCase());
		if(result == 0) {
			result = song1.artist.toLowerCase().compareTo(song2.artist.toLowerCase());
			return result;
		}
		
		//returns the comparison of the two songs
		return result;
	}
}
