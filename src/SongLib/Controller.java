/*
 * 
 * @authors: Chris Hartley and Jerry Zhu
 * 
 */


package SongLib;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.control.ListCell;
import java.util.ArrayList;

import javafx.util.Callback;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class Controller implements Initializable {
    
    //JsonSongData will hold the Json read from file
    JSONObject JsonSongData = new JSONObject();
    
    ObservableList<Song> obsSongList = FXCollections.observableList(new ArrayList<>());
    private int selectedSong = 0;

    //Functional elements in SongLib.fxml
    @FXML
    ListView SongList = new ListView();
    @FXML
    Button EditButt = new Button();
    @FXML
    TextField Title = new TextField();
    @FXML
    TextField Artist = new TextField();
    @FXML
    TextField Album = new TextField();
    @FXML
    TextField Year = new TextField();

    @Override
    public void initialize(URL url, ResourceBundle rb){
        loadJson();
        buildSonglistFromJSON();
        SongList.setItems(obsSongList);
        SongList.getSelectionModel().selectFirst();
        switchSong();
    }
    
    
    //Event Handler for clicking the SongList
    public void switchSong(){

        //No longer saving, so change editButt text back
        EditButt.setText("Edit Song");

        //If there are no songs in list
        if(obsSongList.size() == 0){
            Title.setText("No songs in list");
            Artist.setText("No songs in list");
            Album.setText("No songs in list");
            Year.setText("No songs in list");
            return;
        }
        
        //Set selectedSong to the integer index of the clicked element in the list
        selectedSong = SongList.getSelectionModel().getSelectedIndex();
        
        //Set the Song Details View to the selected song's details
        Title.setText(obsSongList.get(selectedSong).name);
        Artist.setText(obsSongList.get(selectedSong).artist);
        Album.setText(obsSongList.get(selectedSong).album);
        Year.setText(obsSongList.get(selectedSong).year);
    }
    
    
    //Temporary event handler for adding songs
    public void addSong(){
        
        selectedSong = -1;
        /* Use selectedSong = -1 as a flag for edit() to
         * create a new song instead of editing an existing one.
         * This is useful because the flag resets automatically
         * if the user clicks on another song
         */

        //Change editButt text so it is clear we are creating a new song
        EditButt.setText("Save Song");
        
        Title.setText("");
    	Artist.setText("");
    	Album.setText("");
    	Year.setText("");
    	
    	Title.setPromptText("Enter title here");
    	Artist.setPromptText("Enter artist name here");
    	Album.setPromptText("Enter album name here");
    	Year.setPromptText("Enter release year here");
    	
       
    }
    
    
    //Event Handler for deleting
    public void deleteSong(){
        if(obsSongList.size() == 0){
            System.out.println("Warning: Tried to delete song from empty list");
            Alert alert = new Alert(AlertType.INFORMATION);
        	alert.setTitle("Delete Error");
        	alert.setHeaderText(null);
        	alert.setContentText("There are no songs to delete!");
        	alert.showAndWait();
            return;
        }
        
        if(selectedSong == -1){
            //Alert user that they must click on a song to delete
            return;
        }
        
        //Delete the Song object and the observable element
        obsSongList.remove(selectedSong);
        
        //If we are deleting the first song
        if(selectedSong == 0){
            SongList.getSelectionModel().selectFirst(); //select the new first song of the list
        }else{
            SongList.getSelectionModel().selectIndices(selectedSong - 1);
        }
        
        //display the info of newly selected song
        switchSong();
        
    }
    
    //Edit selected song
    public void editSong(){
        
    	
    	//check to see if the song is in the library or not
    	//if it is than change flag accordingly
    	
    	//strings that hold the textfield
    	String textFieldTitle, textFieldArtist;
    	//strings that hold songs in the library
        String songListTitle, songListArtist;
        
        //check to see the textfields are correctly entered
        if(Title.getText().trim().isEmpty()||Artist.getText().trim().isEmpty()) {
        	System.out.println("ERORRR");
        	
        	//if title textfield is empty
        	if(Title.getText().trim().isEmpty()) {
        		Alert alert = new Alert(AlertType.INFORMATION);
            	alert.setTitle("Textfield Error");
            	alert.setHeaderText("Name Textfield Error");
            	alert.setContentText("Please enter a name!");
            	alert.showAndWait();
        	}
        	
        	//if artist textfield is empty
        	else if(Artist.getText().trim().isEmpty()) {
        		Alert alert = new Alert(AlertType.INFORMATION);
            	alert.setTitle("Textfield Error");
            	alert.setHeaderText(null);
            	alert.setContentText("Please enter an artist!");
            	alert.showAndWait();
        	}
        	
        	//if both textfields are empty
        	else {
        		Alert alert = new Alert(AlertType.INFORMATION);
            	alert.setTitle("TextField Error");
            	alert.setHeaderText(null);
            	alert.setContentText("Title and Artist need to be filled out!");
            	alert.showAndWait();
        	}
        }
        
        else {
	        textFieldTitle = Title.getText().trim();
	        textFieldArtist = Artist.getText().trim();
	        
	        for(Song s: obsSongList) {
	        	//check the current songs name and artist
	        	songListTitle = s.name;
	        	songListArtist = s.artist;
	        	
	        	//System.out.println(textFieldTitle + "," + songListTitle);
	        	//System.out.println(textFieldArtist + "," + songListArtist);
	        	
	        	//check if input is duplicate
	        	if((songListTitle.equalsIgnoreCase(textFieldTitle) && songListArtist.equalsIgnoreCase(textFieldArtist)) && (s.album.equalsIgnoreCase(Album.getText()) && s.year.equalsIgnoreCase(Year.getText()))) {
	        		Alert alert = new Alert(AlertType.INFORMATION);
	            	alert.setTitle("Duplicate Error");
	            	alert.setHeaderText(null);
	            	alert.setContentText("This song is already in the library!");
	            	alert.showAndWait();
	            	return;
	        	}


	        }
	    	
	    	if(selectedSong == -1){ //flag that we are saving a new song. Gets set in addSong()
	            //Create and save song
	    		Song newSong = new Song(Title.getText().trim(),Artist.getText().trim(),Album.getText().trim(),Year.getText().trim());
	    		obsSongList.addAll(newSong);

	    		//sets the selected song as the last element
	    		SongList.getSelectionModel().selectLast();
	    		
	    		//sort the list
	    		FXCollections.sort(obsSongList, new SongComparator());
	    		
	        }else{
	            //Update selected song object in obsSongList
	        	//obsSongList.get(selectedSong).setDetails(Title.getText(),Artist.getText(),Album.getText(),Year.getText());
	        	Song editSong = new Song(Title.getText().trim(),Artist.getText().trim(),Album.getText().trim(),Year.getText().trim());
	        	obsSongList.set(selectedSong, editSong);
	        	
	        	//sort the list
	        	FXCollections.sort(obsSongList, new SongComparator());
	        	
	        	
	        }

	        //Will display updated details
	        switchSong();
        }
    }
    
    public void loadJson(){
        //Generate some test JSON. Will not be in final method
        /*
    	JSONObject testSong = new JSONObject();
        JSONObject testSongData = new JSONObject();
    
        testSongData.put("title", "Gangnam Style");
        testSongData.put("artist", "Psy");
        testSongData.put("album", "Gangnam Style (single)");
        testSongData.put("year", "2015");
    
        testSong.put("Gangnam Style", testSongData);
        JsonSongData.put("Psy", testSong);
    
        testSong = new JSONObject();
        testSongData = new JSONObject();
    
        testSongData.put("title", "Gangname Fashion");
        testSongData.put("artist", "Psych");
        testSongData.put("album", "Gangname Fashion (double)");
        testSongData.put("year", "3015");
    
        testSong.put("Gangnamii Style", testSongData);
        JsonSongData.put("Psych", testSong);
        */
        //actual method body goes here
    	JSONParser parser = new JSONParser();
    	 
        try {
 
            Object obj = parser.parse(new FileReader(
                    "./src/SongLib/songList.json"));
 
            JsonSongData = (JSONObject) obj;
            

 
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    
    public void buildSonglistFromJSON(){
        System.out.println("Info: Loading songs from JSON");
        
        for (Object key: JsonSongData.keySet()){
            String artist = (String)key;
            System.out.println("Info: Loading songs by " + artist);
            buildSongsByArtist((JSONObject) JsonSongData.get(artist));
        }
        
	    FXCollections.sort(obsSongList, new SongComparator());

    }

    public void buildSongsByArtist(JSONObject artistSongs){
        for (Object key: artistSongs.keySet()){
            String song = (String)key;
            buildSongFromJSON((JSONObject) artistSongs.get(song));
        }
    }

    public void buildSongFromJSON(JSONObject jsonSong){
        String title = (String)jsonSong.get("title");
        String artist = (String)jsonSong.get("artist");
        String album = (String)jsonSong.get("album");
        String year = (String)jsonSong.get("year");
        Song song = new Song(title, artist, album, year);
        obsSongList.addAll(song);
    }

}
