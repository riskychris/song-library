/*
 * 
 * @authors: Chris Hartley and Jerry Zhu
 * 
 */
package SongLib;

import javafx.application.Application;
import javafx.scene.control.ListView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.FileWriter;

import org.json.simple.JSONObject;

public class Main extends Application {
    private Scene primaryScene;
    
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("SongLib.fxml"));
        primaryStage.setTitle("Song Library by Chris Hartley and Jerry Zhu");
        this.primaryScene = new Scene(root, 275, 400);
        primaryStage.setScene(this.primaryScene);
        primaryStage.show();

        JSONObject testSongData = new JSONObject();
        testSongData.put("test", "hi");
    }
    
    public void stop(){
    	System.out.println("saving to ./src/songList.json...");
        ListView<Song> SongListView= (ListView<Song>) primaryScene.lookup("#SongList");
        JSONObject JsonSongData = new JSONObject();
        for(Song song : SongListView.getItems() ){
            System.out.println(song);
            
            JSONObject saveSong = new JSONObject();
            JSONObject saveSongData = new JSONObject();
            
            saveSongData.put("title", song.name);
            saveSongData.put("artist", song.artist);
            saveSongData.put("album", song.album);
            saveSongData.put("year", song.year);
            
            saveSong.put(song.name,saveSongData);
            JsonSongData.put(song.artist,saveSong);
            
        }
        try {
        	FileWriter fileWriter = new FileWriter("./src/SongLib/songList.json");
        	fileWriter.write(JsonSongData.toJSONString());
            fileWriter.close();
            
            System.out.println("saving complete!");
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
